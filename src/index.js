import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import reportWebVitals from './reportWebVitals';
import {createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './reducers';
import {Provider} from 'react-redux'
import createSagaMiddleware from 'redux-saga'
import { watcherSaga } from './reducers/sagas/rootSaga'
//SAGA
const sagaMiddleware = createSagaMiddleware();
const middleware = [sagaMiddleware]
const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
// REDUCER
// STORE GLOBAL STATE
const store = createStore(rootReducer,composeEnhancer(applyMiddleware(...middleware)));
sagaMiddleware.run(watcherSaga)
// for (var i = 1; i <= 10; i++) {
//   store.dispatch(increment());
// }
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();

// ACTION INCREAMENT

export const initialPosition = () => {
  return {
    type: 'INITIALIZEPOSITION'
  }
}

export const randomizePosition = () => {
  return {
    type: 'RANDOMIZEPOSITION'
  }
}

export const switchPosition = (index) => {
  return {
    type: 'SWITCHTILE',
    tileIndex: index
  }
}

export const GET_INITIALSTATE = () => {
  return {
    type: 'GET_INITIALSTATE'
  }
}

export const SET_INITIALSTATE = () => {
  return {
    type: 'SET_INITIALSTATE'
  }
}
export const SET_RECORDSTATE = (payload) => {
  return {
    type: 'SET_RECORDSTATE',
    payload : payload
  }
}

export const increment = () => {
  return {
    type: 'INCREMENT'
  }
}

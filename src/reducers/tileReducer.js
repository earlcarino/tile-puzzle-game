import produce from 'immer'

export const GET_INITIALSTATE = "GET_INITIALSTATE";
const SET_INITIALSTATE = "SET_INITIALSTATE";
export const SET_RECORDSTATE = "SET_RECORDSTATE";

export const setInitialState = (data) => ({
  type: SET_INITIALSTATE,
  payload: data
});

export const setRecordState = (data) => ({
  type: SET_RECORDSTATE,
  payload: data
});


const movements = []
const defaultVacantIndex = 8;
var vacantIndexVal = []
var vacantIndex = []
var tileToSwitchVal = []
var tiles = []
var defaultTile = []
var p = []
var t = []
var isValidMovement = null
const initialState ={"tile":[],"winTile":[],"isStarted":false,"moveCount":0,"movements":[],"defaultVacantIndex":"","isWin":false};

const tileReducer = (state = initialState, action) => {
  switch (action.type) {
    case 'RANDOMIZEPOSITION':
        var t = state.tile.slice().sort((a, b) => Math.random() - 0.5);
        var vacantIndex = t.findIndex(x => x.tileNo === 9);
        var sort = {tile:t,
                    isStarted:true,
                    moveCount:0,
                    movements:state.movements,
                    winTile:state.winTile,
                    defaultVacantIndex:vacantIndex,
                    isWin:false};
        p =  produce(state, () => sort);
        return p;
    case 'SWITCHTILE':
        vacantIndex = state.tile.findIndex(x => x.tileNo === 9);
        vacantIndexVal = state.tile[vacantIndex];
        tileToSwitchVal = state.tile[action.tileIndex];
        tiles = state.tile;
        defaultTile = JSON.stringify(state.winTile);
        isValidMovement = action.tileIndex-state.defaultVacantIndex
        if(isValidMovement == state.movements.top || isValidMovement == state.movements.left || isValidMovement == state.movements.rigth || isValidMovement == state.movements.bottom) {
            p =  produce(state, draftState => {
                            var t = produce(draftState.tile, tileDraft => {
                              tileDraft[action.tileIndex] = tileDraft[vacantIndex]
                              tileDraft[vacantIndex] = state.tile[action.tileIndex]
                            });

                            var newTilePosition = JSON.stringify(t);
                            var isWin = false;
                            var isStarted = state.isStarted;
                            if(defaultTile == newTilePosition) {
                              isWin = true;
                              isStarted = false;
                            }
                            state = {tile:t,
                                      isStarted:isStarted,
                                      moveCount:state.moveCount+1,
                                      movements:state.movements,
                                      winTile:state.winTile,
                                      defaultVacantIndex:action.tileIndex,
                                      isWin:isWin};
                                      console.log(state)
                            return state;
                          });
        }
        else {
          p = state;
        }
        return p;
    case 'GET_INITIALSTATE':
    break;
    case 'SET_RECORDSTATE':
    break;
    case 'SET_INITIALSTATE':
      t = action.payload;
      t.winTile = action.payload.tile;
      p =  produce(state, () => t);
      return p;
    break;
    default:
        p =  produce(state, () => action.payload);
        return p;
  }

  return state
};

export default tileReducer;

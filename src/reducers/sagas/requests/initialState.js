import axios from 'axios'

export function requestGetInitialState() {
  return axios.request({
    method:'get',
    type: 'json',
    url: 'http://localhost:3000/initialState.json'
  })
}

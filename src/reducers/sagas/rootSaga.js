import { takeLatest } from 'redux-saga/effects'
import { handleGetInitialState } from './handlers/initialState'
import { handleSetRecordState } from './handlers/setRecordState'
import { GET_INITIALSTATE, SET_RECORDSTATE } from './../tileReducer'

export function* watcherSaga() {
  yield takeLatest(GET_INITIALSTATE, handleGetInitialState)
  yield takeLatest(SET_RECORDSTATE, handleSetRecordState)
}

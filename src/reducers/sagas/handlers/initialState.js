import {call ,put} from 'redux-saga/effects'
import { getInitialState,setInitialState } from './../../tileReducer'
import { requestGetInitialState } from '../requests/initialState'

export function* handleGetInitialState(action) {
  try {
    const response = yield call(requestGetInitialState)
    const { data } = response
    yield put(setInitialState(data))
  } catch (e) {
    console.log(e);
  }
}

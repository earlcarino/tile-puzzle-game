import {call ,put} from 'redux-saga/effects'
import { getRecordState,setRecordState } from './../../tileReducer'
import { requestSetRecordState } from '../requests/setRecordState'

export function* handleSetRecordState(action) {
  try {
    const response = yield call(requestSetRecordState)
    const { data } = response
    yield put(setRecordState(data))
  } catch (e) {
    console.log(e);
  }
}

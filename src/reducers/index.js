import counter from "./counterReducer";
import tileReducer from "./tileReducer";
import { combineReducers } from "redux";

const rootReducer = combineReducers({
  counterReducer : counter,
  tileReducer : tileReducer,
});

export default rootReducer;

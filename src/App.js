import React, { useEffect } from 'react';
import ReactDOM from 'react-dom';
import {useSelector, useDispatch} from 'react-redux';
import {increment,randomizePosition,initialPosition,switchPosition,GET_INITIALSTATE,SET_RECORDSTATE} from './actions';
import './App.css'
import produce from 'immer'

function App() {
  const dispatch = useDispatch();
  const tilesCount = useSelector(state => state.tileReducer);
  const counter = useSelector(state => state.counterReducer);
  const state = useSelector(state => state);

  useEffect(() => {
    dispatch(GET_INITIALSTATE())
  }, []);

  function startGame(x) {
    dispatch(randomizePosition())
  }

  function endGame() {
    dispatch(GET_INITIALSTATE())
  }

  function switchTile(tileIndex) {
    dispatch(switchPosition(tileIndex))
  }

  let winh = tilesCount.isWin ? <div className="win">Congratiolations! You solved the puzzle!</div> : false;

  return (
    <div className="App">
      <button className={tilesCount.isStarted ? "btn endbtn" : "btn starbtn"} onClick={tilesCount.isStarted ? () => endGame() : () => startGame()}>{tilesCount.isStarted ? "END GAME" : "START GAME"}</button>
      <div>Move Count: {tilesCount.moveCount} </div>
      <div className="container">
          <ul>
            {
                tilesCount.tile.map((tileNumber,index) => {
                if(tileNumber.tileNo < 9)
                    return  <li className='tile' key={index} onClick={tilesCount.isStarted ? () => switchTile(index) : () => false}>{tileNumber.tileNo}</li>
                else
                    return  <li className='tile blank' key={index} ></li>
                })
            }
          </ul>
      </div>
           {winh}
    </div>
  );
}

export default App;
